#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/timer.h>

#define PWM_CH_1_PORT GPIOA
#define PWM_CH_1_PIN 	GPIO6
#define PWM_CH_2_PORT GPIOA
#define PWM_CH_2_PIN 	GPIO7
#define PWM_CH_3_PORT GPIOB
#define PWM_CH_3_PIN 	GPIO1

#define PERIOD	0x7F
#define NUM_OF_CH 3

typedef enum PwmCh_ {
  PWM_CH1 = TIM_OC1,
  PWM_CH2 = TIM_OC2,
  PWM_CH3 = TIM_OC4 
} PwmCh;

void PWM_init(void);
void PWM_startTimer(void);
void PWM_stopTimer(void);
void PWM_enable(PwmCh ch);
void PWM_enableAll(void);
void PWM_disable(PwmCh ch);
void PWM_disableAll(void);

//Sets the pulsewidth of the channel, from 0...127 
void PWM_set(PwmCh ch, uint8_t pulse_width);
