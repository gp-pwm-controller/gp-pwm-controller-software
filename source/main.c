#include "Leds.h"
#include "PWM.h"
#include <libopencm3/stm32/gpio.h>

#define MAX_CH_VALUE 		4
#define SETTING_TIMEOUT	400000
#define ENC_PORT				GPIOA
#define ENC_RCC					RCC_GPIOA
#define ENC_A						GPIO4
#define ENC_B						GPIO5
#define ENC_S						GPIO9

typedef enum _Event_ {NO_EVENT, EV_CW_ROT, EV_CCW_ROT, EV_BUTTON_PRESS, EV_TIME_ELAPSED} Event;
typedef enum _State_ {ST_IDLE, ST_OPERATING, ST_SETTING} State;
typedef enum _Channel_ {CH1, CH2, CH3} Channel;

volatile Event event;

Channel getNextCh(Channel current_ch);
uint8_t getPwmValue(uint8_t ch_value);
PwmCh getPwmCh(Channel ch);
void lightLeds(uint8_t ch_values[]);
void lightLedsOperation(uint8_t ch_values[]);
void lightLedsSetting(Channel setting_ch, uint8_t ch_values[]);
void disableLeds(void);
void initEncoder(void);

int main(void)
{
	State state;
	uint8_t ch_values[3];
	uint8_t setting_ch;
	uint16_t val_enc_a, val_enc_a_old;
	uint16_t val_enc_s, val_enc_s_old;
	uint32_t setting_timeout;
	PWM_init();
	Leds_init();
	initEncoder();

	PWM_disableAll();

	state = ST_IDLE;
	event = NO_EVENT;
	setting_ch = CH1;
	for (Channel ch = 0; ch < sizeof(ch_values)/sizeof(ch_values[0]); ch++)
	{
		ch_values[ch] = 0;
		PWM_set(getPwmCh(ch), getPwmValue(ch_values[ch]));
	}
	val_enc_a = val_enc_a_old = gpio_get(ENC_PORT, ENC_B);
	val_enc_s = val_enc_s_old = gpio_get(ENC_PORT, ENC_S);
	setting_timeout = 0;
	while(1)
	{
		val_enc_a = gpio_get(ENC_PORT, ENC_B);
		if (val_enc_a != val_enc_a_old)
		{
			if (val_enc_a == 0)
			{
				if (gpio_get(ENC_PORT, ENC_A))
				{
					event = EV_CCW_ROT;
				}
				else
				{
					event = EV_CW_ROT;
				}
			}
			val_enc_a_old = val_enc_a;
		}

		val_enc_s = gpio_get(ENC_PORT, ENC_S);
		if (val_enc_s != val_enc_s_old)
		{
			if (val_enc_s == 0)
			{
				event = EV_BUTTON_PRESS;
			}
			val_enc_s_old = val_enc_s;
		}

		switch (state)
		{
		case ST_IDLE:
			if (event == EV_BUTTON_PRESS)
			{
				event = NO_EVENT;
				state = ST_OPERATING;
				lightLedsOperation(ch_values);
				PWM_enableAll();
			}
			break;
		case ST_OPERATING:
			if (event == EV_BUTTON_PRESS)
			{
				event = NO_EVENT;
				state = ST_IDLE;
				PWM_disableAll();
				disableLeds();
			}
			else if (event == EV_CW_ROT || event == EV_CCW_ROT)
			{
				event = NO_EVENT;
				state = ST_SETTING;
				lightLedsSetting(setting_ch, ch_values);
				setting_timeout = 0;
			}
			break;
		case ST_SETTING:
			if (setting_timeout++ >= SETTING_TIMEOUT)
			{
				event = EV_TIME_ELAPSED;
				setting_timeout = 0;
			}
			if (event == EV_TIME_ELAPSED)
			{
				event = NO_EVENT;
				state = ST_OPERATING;
				lightLedsOperation(ch_values);
			}
			else if (event == EV_BUTTON_PRESS)
			{
				event = NO_EVENT;
				setting_ch = getNextCh(setting_ch);
				lightLedsSetting(setting_ch, ch_values);
				setting_timeout = 0;
			}
			else if (event == EV_CW_ROT)
			{
				event = NO_EVENT;
				if (ch_values[setting_ch] < MAX_CH_VALUE)
				{
					ch_values[setting_ch]++;
				}
				PWM_set(getPwmCh(setting_ch), getPwmValue(ch_values[setting_ch]));
				lightLedsSetting(setting_ch, ch_values);
				setting_timeout = 0;
			}
			else if (event == EV_CCW_ROT)
			{
				event = NO_EVENT;
				if (ch_values[setting_ch] > 0)
				{
					ch_values[setting_ch]--;
				}
				PWM_set(getPwmCh(setting_ch), getPwmValue(ch_values[setting_ch]));
				lightLedsSetting(setting_ch, ch_values);
				setting_timeout = 0;
			}
			break;
		}
	}
}

Channel getNextCh(Channel current_ch)
{
	if (current_ch == CH3)
	{
		return CH1;
	}
	else
	{
		return current_ch+1;
	}
}

uint8_t getPwmValue(uint8_t ch_value)
{
	return (PERIOD / MAX_CH_VALUE) * ch_value;
}

PwmCh getPwmCh(Channel ch)
{
	if (ch == CH1)
	{
		return PWM_CH1;
	}
	else if (ch == CH2)
	{
		return PWM_CH2;
	}
	else
	{
		return PWM_CH3;
	}
}

void lightLeds(uint8_t ch_values[])
{
	for (uint8_t ch = 0; ch < 3; ch++)
	{
		uint8_t led_value = 0;
		for (uint8_t i = ch_values[ch]; i > 0; i--)
		{
			led_value |= 0x1 << (i - 1);
		}
		Leds_setGroup(ch+1, led_value);
	}
	Leds_update();
}

void lightLedsOperation(uint8_t ch_values[])
{
	Leds_setGroup(CH_INDICATOR, 0x00);
	lightLeds(ch_values);
	Leds_update();
}

void lightLedsSetting(Channel setting_ch, uint8_t ch_values[])
{
	Leds_setGroup(CH_INDICATOR, 0x1 << setting_ch);
	lightLeds(ch_values);
	Leds_update();
}

void disableLeds(void)
{
	uint8_t led_values_off[4] = {0};
	Leds_set(led_values_off);
	Leds_update();
}

void initEncoder(void)
{
	rcc_periph_clock_enable(ENC_RCC);
  gpio_mode_setup(ENC_PORT, GPIO_MODE_INPUT, GPIO_PUPD_PULLUP, ENC_A);
	gpio_mode_setup(ENC_PORT, GPIO_MODE_INPUT, GPIO_PUPD_PULLUP, ENC_B);
	gpio_mode_setup(ENC_PORT, GPIO_MODE_INPUT, GPIO_PUPD_PULLUP, ENC_S);
}