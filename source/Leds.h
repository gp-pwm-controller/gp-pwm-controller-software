#ifndef LEDS_H_
#define LEDS_H_

#include <inttypes.h>
#include <libopencm3/stm32/gpio.h>

#define PORT_SR 	GPIOA
#define RCC_SR		RCC_GPIOA
#define PIN_SER		GPIO0
#define PIN_STR		GPIO1
#define PIN_SCLR	GPIO2
#define PIN_RCK		GPIO3

#define NUM_OF_GROUPS 4

typedef enum LedGroup_ {
  CH_INDICATOR,
  CH_1,
  CH_2,
  CH_3
} LedGroup;

void Leds_init(void);
void Leds_update(void);
void Leds_setGroup(LedGroup group, uint8_t val);
void Leds_set(uint8_t* const vals);
uint8_t Leds_getGroup(LedGroup group);
uint8_t* Leds_get(void);

#endif