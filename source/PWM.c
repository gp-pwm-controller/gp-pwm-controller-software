#include "PWM.h"
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/timer.h>

static PwmCh channels[NUM_OF_CH] = {PWM_CH1, PWM_CH2, PWM_CH3};

void PWM_init(void)
{
	rcc_periph_clock_enable(RCC_TIM3);
	rcc_periph_clock_enable(RCC_GPIOA);
	rcc_periph_clock_enable(RCC_GPIOB);

	//configure the PWM output channel
	gpio_mode_setup(PWM_CH_1_PORT, GPIO_MODE_AF, GPIO_PUPD_NONE, PWM_CH_1_PIN);
	gpio_set_output_options(PWM_CH_1_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_50MHZ, PWM_CH_1_PIN);
	gpio_mode_setup(PWM_CH_2_PORT, GPIO_MODE_AF, GPIO_PUPD_NONE, PWM_CH_2_PIN);
	gpio_mode_setup(PWM_CH_3_PORT, GPIO_MODE_AF, GPIO_PUPD_NONE, PWM_CH_3_PIN);

	//configure alternate function of PWM output channel to the timer channel
	gpio_set_af(PWM_CH_1_PORT, GPIO_AF1, PWM_CH_1_PIN);
	gpio_set_af(PWM_CH_2_PORT, GPIO_AF1, PWM_CH_2_PIN);
	gpio_set_af(PWM_CH_3_PORT, GPIO_AF1, PWM_CH_3_PIN);

	//TIM3 config
	timer_set_clock_division(TIM3, 0);
	timer_set_prescaler(TIM3, 0);
	timer_set_period(TIM3, PERIOD);
	timer_direction_up(TIM3);

	for (uint8_t i = 0; i < NUM_OF_CH; i++)
	{
		timer_set_oc_mode(TIM3, channels[i], TIM_OCM_PWM1);
		timer_set_oc_idle_state_unset(TIM3, channels[i]);
		timer_set_oc_polarity_high(TIM3, channels[i]);
		timer_set_oc_value(TIM3, channels[i], 0);
		timer_enable_oc_preload(TIM3, channels[i]);
		timer_disable_oc_output(TIM3, channels[i]);
	}
	timer_enable_preload(TIM3);
	timer_enable_counter(TIM3);
}

void PWM_startTimer(void)
{
	timer_enable_counter(TIM3);
}

void PWM_stopTimer(void)
{
	timer_disable_counter(TIM3);
}

void PWM_enable(PwmCh ch)
{
	timer_enable_oc_output(TIM3, ch);
}

void PWM_enableAll(void)
{
	for (uint8_t i = 0; i < NUM_OF_CH; i++)
	{
		timer_enable_oc_output(TIM3, channels[i]);
	}
}

void PWM_disable(PwmCh ch)
{
	timer_disable_oc_output(TIM3, ch);
}

void PWM_disableAll(void)
{
	for (uint8_t i = 0; i < NUM_OF_CH; i++)
	{
		timer_disable_oc_output(TIM3, channels[i]);
	}
}

void PWM_set(PwmCh ch, uint8_t pulse_width)
{
	timer_set_oc_value(TIM3, ch, pulse_width);
}




