#include "Leds.h"
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>

const uint8_t GROUPE_SIZE[4] = {3, 4, 4, 4};

static uint8_t led_values[NUM_OF_GROUPS];

uint16_t getRawLedValues(void);

uint16_t getRawLedValues(void)
{
  uint16_t raw = 0x0000;
  raw |= (led_values[CH_1] & 0x0F);
  raw |= (led_values[CH_2] & 0x0F) << 4;
  raw |= (led_values[CH_3] & 0x0F) << 8;
  //Channel indicator LEDs are flipped on the PCB
  raw |= (led_values[CH_INDICATOR] & 0x04) << 10;
  raw |= (led_values[CH_INDICATOR] & 0x02) << 12;
  raw |= (led_values[CH_INDICATOR] & 0x01) << 14;
  return raw;
}

void Leds_init(void)
{
  rcc_periph_clock_enable(RCC_SR);
  gpio_mode_setup(PORT_SR, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, PIN_SER);
	gpio_mode_setup(PORT_SR, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, PIN_STR);
	gpio_mode_setup(PORT_SR, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, PIN_SCLR);
	gpio_mode_setup(PORT_SR, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, PIN_RCK);

	gpio_set(PORT_SR, PIN_SCLR);
	gpio_clear(PORT_SR, PIN_SER);
  gpio_clear(PORT_SR, PIN_STR);
  gpio_clear(PORT_SR, PIN_RCK);

  for (uint8_t i = 0; i < NUM_OF_GROUPS; i++)
  {
    led_values[i] = 0x00;
  }
  Leds_update();
}

void Leds_update(void)
{
  uint16_t vals_raw = getRawLedValues();

	gpio_clear(PORT_SR, PIN_RCK);

  for (uint8_t i = 0; i < 15; i++)
  {
    gpio_clear(PORT_SR, PIN_STR);
    if (vals_raw & (0x01 << 14))
    {
      gpio_set(PORT_SR, PIN_SER);
    }
    else
    {
      gpio_clear(PORT_SR, PIN_SER);
    }
    gpio_set(PORT_SR, PIN_STR);
    vals_raw <<= 1;
  }

  gpio_set(PORT_SR, PIN_RCK);
  gpio_clear(PORT_SR, PIN_RCK);
}

void Leds_setGroup(LedGroup group, uint8_t val)
{
  led_values[group] = val;
}

void Leds_set(uint8_t* const vals)
{
  for (uint8_t i = 0; i < NUM_OF_GROUPS; i++)
  {
    led_values[i] = vals[i];
  }
}

uint8_t Leds_getGroup(LedGroup group)
{
  return led_values[group];
}

uint8_t* Leds_get(void)
{
  return led_values;
}