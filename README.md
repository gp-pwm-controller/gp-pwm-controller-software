# GP PWM Controller Software

STM32 software of a general purpose PWM controller with 3 channels.  

## Set up

Run the following commands to get the project up and running:
```bash
 git clone --recurse-submodules git@gitlab.com:gp-pwm-controller/gp-pwm-controller-software.git
 cd gp-pwm-controller-software
 make -C libopencm3 # (Only needed once)
 make -C source
```

If you have an older git, or got ahead of yourself and skipped the `--recurse-submodules`
you can fix things by running `git submodule update --init` (this is only needed once).

## Development

For development, you can connect the PCB via the SWD interface with a ST-link V2 board and run the command `make flash` within the `source` directory. This will build the project and flash it to the STM32 micro.